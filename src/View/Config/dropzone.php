<?php

return [
	'rules_files_formats' => 'required|mimes:jpg,jpeg,png',
	'message'=>[
		'bad_mimes' => 'Uploaded file is not in a good format'
	]
];
