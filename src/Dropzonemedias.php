<?php
namespace Kryzalid\DropzoneMedias;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;

class Dropzonemedias
{
    public static $rules = [
        'file' => 'required|mimes:png,jpeg,jpg,gif'
    ];
    public static $messages = [
        'file.mimes' => 'Uploaded file is not in a good format',
        'file.required' => 'Please, select a file'
    ];

	/**
	 * Upload image ajax query
	 * @param  object $form_data
	 * @return json
	 */
    public function upload( $form_data )
    {
		self::$rules['file'] = Config::get('dropzone.rules_files_formats');
		self::$messages['file.mimes'] = Config::get('dropzone.message.bad_mimes');
		
        $validator = Validator::make($form_data, self::$rules, self::$messages);
        if ($validator->fails()) {
            return response()->json([
                'error' => true,
                'message' => $validator->messages()->first(),
                'code' => 400
            ], 400);
        }
        $photo = $form_data['file'];
        $objMedia = new $form_data['model'];
        $objMedia->kdz_setImageAttribute($photo, class_basename($form_data['model']));
		$form_data['media_name'] = $photo->getClientOriginalName();
        foreach ($form_data as $key => $value) {
            if ($key != 'file' && $key != 'agent' && $key != 'model') {
                $objMedia->setAttribute($key, $value);
            }
        }
        $objMedia->save();
        $originalName = $objMedia->kdz_getMediaName();
        return response()->json([
            'error' => false,
            'code'  => 200,
            'filename' => $originalName,
            'id' => $objMedia->id,
        ], 200);
    }

    /**
     * Delete Image From Session folder, based on server created filename
     * @param  array $args
     * @return json
     */
    public function delete( $args )
    {
        $objMedia = new $args['model'];
        $objMedia = $objMedia::find($args['id']);
        if(empty($objMedia))
        {
            return response()->json([
                'error' => true,
                'code'  => 400
            ], 400);
        }
        Storage::disk($objMedia->disk)->delete($objMedia->kdz_getPath());
        if(!empty($objMedia))
        {
            $objMedia->delete();
        }
        return response()->json([
            'error' => false,
            'code'  => 200
        ], 200);
    }
}
