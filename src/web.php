<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes For Ajax Query
|--------------------------------------------------------------------------
|*/
Route::group(['prefix' => 'admin', 'namespace' => 'Kryzalid\DropzoneMedias'], function()
{
	Route::get('/', ['as' => 'dropzoneMedias', 'uses' => 'DropzonemediasController@index']);
	Route::post('/uploadMedia', ['as' => 'upload_post', 'uses' =>'DropzonemediasController@store']);
	Route::post('/uploadMedia/delete', ['as' => 'upload_remove', 'uses' =>'DropzonemediasController@destroy']);
});
