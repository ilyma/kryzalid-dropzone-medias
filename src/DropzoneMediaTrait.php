<?php

namespace Kryzalid\DropzoneMedias;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Config;
use Intervention\Image\ImageManager;

trait DropzoneMediaTrait
{
	/**
	 * NAME OF DISK STORAGE (DEFAULT VALUE = public)
	 */
	public $disk = 'medias';


	/**
    * Mutator for file upload image_before
    * @param [type] $value [description]
    */
    public function kdz_setImageAttribute($value, $modelName)
    {
        $this->kdz_imageMutator($modelName, 'path', $value);
    }

	/**
    * Mutator for file upload image_before
    * @param [type] $value [description]
    */
    public function kdz_getPath()
    {
        return $this->path;
    }
	/**
    * Mutator for file upload image_before
    * @param [type] $value [description]
    */
    public function kdz_getMediaName()
    {
        return $this->mediaName;
    }

	/*
	|--------------------------------------------------------------------------
	| MUTATORS FOR IMAGE IMPORT
	|--------------------------------------------------------------------------
	*/
	public function kdz_imageMutator($attribute, $attribute_name, $value)
	{
		$destination_path = $attribute . DIRECTORY_SEPARATOR;
		// if the image was erased
		if ($value==null) {
			// delete the image from disk
			Storage::disk($this->disk)->delete($this->attributes[$attribute_name]);
			// set null in the database column
			$this->attributes[$attribute_name] = null;
		}
		// 0. Make the image
		$image = \Image::make($value);
		// 1. Generate a filename.
		$filename = md5($value.microtime()).'.jpg';
		// 2. Store the image on disk.
		Storage::disk($this->disk)->put($destination_path.$filename, $image->stream());
		// 3. Save the path to the database
		$this->attributes[$attribute_name] = $destination_path.$filename;
	}
}
