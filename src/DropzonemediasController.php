<?php

namespace Kryzalid\DropzoneMedias;

use Kryzalid\DropzoneMedias\Dropzonemedias;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class DropzonemediasController extends Controller
{
    protected $dropzone;

    public function __construct(DropzoneMedias $dropzoneMedia)
    {
        $this->dropzone = $dropzoneMedia;
    }

    public function index()
    {
        return view('welcome');
    }

    public function store()
    {
        $photo = Input::all();
        $response = $this->dropzone->upload($photo);
        return $response;

    }

    public function destroy()
    {
        $filename = Input::get('id');
        if(!$filename)
        {
            return 0;
        }
        $response = $this->dropzone->delete( $filename );
        return $response;
    }
}
