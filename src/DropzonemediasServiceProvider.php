<?php

namespace Kryzalid\DropzoneMedias;

use Illuminate\Support\ServiceProvider;

class DropzonemediasServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
		// Copy for all dependencies file (view, config, css, js)
		$kdz_migration_base = [__DIR__.'/View/Migrations' => $this->app->databasePath() . '/migrations'];
		$kdz_config_files = [__DIR__.'/View/Config' => config_path()];
		$kdz_custom_field_view = [__DIR__.'/View/SourceFiles/upload_multiple_dropzone.blade.php' => resource_path('views/vendor/backpack/crud/fields/upload_multiple_dropzone.blade.php')];
		$kdz_css = [__DIR__.'/View/SourceFiles/dropzone.css' => public_path('css/dropzone.css')];
		$kdz_js = [__DIR__.'/View/SourceFiles/dropzone.min.js' => public_path('js/dropzone.min.js')];

		$this->publishes($kdz_config_files, 'min-config');
		$this->publishes($kdz_custom_field_view, 'min-config');
		$this->publishes($kdz_css, 'min-config');
		$this->publishes($kdz_js, 'min-config');
		$this->publishes($kdz_migration_base, 'min-config');

		// add the root disk to filesystem configuration
		app()->config['filesystems.disks.medias'] = [
			'driver' => 'local',
			'root'   => base_path(),
			'driver' => 'local',
			'root' => public_path('medias'),
			'url' => env('APP_URL'),
			'visibility' => 'public',
		];

		// In installation Package
		if ($this->app->runningInConsole()) {
			$this->commands([
				\Kryzalid\DropzoneMedias\Commands\KdzInstall::class
			]);
		}
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        include __DIR__.'/web.php';
		$this->app->make('Kryzalid\DropzoneMedias\DropzonemediasController');
    }
}
