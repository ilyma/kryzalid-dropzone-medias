<?php

namespace Kryzalid\DropzoneMedias\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;
class KdzInstall extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'kdz:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command or install Kryzalid dropzone package in Laravel-Backpack';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		$this->info("Copy minimum file in public folder");
		$this->commandExecute('php artisan vendor:publish --provider="Kryzalid\DropzoneMedias\DropzonemediasServiceProvider" --tag=min-config');
		$this->info("Create table in database");
		// $this->info('Start the interview');

		$this->commandExecute('php artisan migrate');
		$this->info("Thank's for your execution");
    }

	/**
	 * Execute command in variable
	 * @param  [string] $command
	 */
	public function commandExecute($command)
	{
		$process = new Process($command, null, null, null, 300, null);
		$process->run(function ($type, $buffer) {
			if (Process::ERR === $type) {
				$this->line($buffer);
			} else {
				$this->line($buffer);
			}
		});
		if (!$process->isSuccessful()) {
			throw new ProcessFailedException($process);
		}
	}


}
