# Kryzalid DropZone package

It's a package for upload multi image in drag and drop in admin of Laravel with BackPack.

## Getting Started

### Prerequisites

For installing and using package
- PHP 7.x
- Composer
- Laravel 5.5 or more
- BackPack 3.2 or more
- DropzoneJS

### Installing

This page assumes
- You have composer as global command
- You have a working instalation of [Laravel v5.5 or more](https://laravel.com/docs/5.5#installing-laravel)
- And you have installing [BackPack for Laravel](https://laravel-backpack.readme.io/v3.4/docs/install-on-laravel-56) (min v3.3) Base and Crud package

Add this lines in your composer install
```
"repositories": [
	{
    	"type": "vcs",
    	"url": "https://ilyma@bitbucket.org/ilyma/kryzalid-dropzone-medias.git"
    }
]
```
and execute
```
composer require kryzalid/dropzonemedias
```
And after
```
php artisan kdz:install
```
This script run a migrate for adding medias table in your DB, copie custom field view in backpack folder and add CSS and JS for DropzoneJS plugin. A config file is adding in the config folder of your project.

Now, we use backpack crud command for created model, controller and request files
```
php artisan backpack:crud media
```
## In Backpack files
In Model/Media.php

```
...
use Kryzalid\DropzoneMedias\DropzoneMediaTrait;

class Media extends Model
{
    ...
	use DropzoneMediaTrait;
...
```
In Model/Post.php // Or your object
```
...
/*
|--------------------------------------------------------------------------
| RELATIONS
|--------------------------------------------------------------------------
*/
/**
* Get medias for this post
*/
public function gallery()
 {
	 return $this->hasMany('App\Models\Media', 'fk_post_id', 'id');
 }
 ...
 ```
 In your Crud view add fields
 ```
[
    'label' => 'Images',
	'type' => 'upload_multiple_dropzone', // the name for vue
	'name' => 'path', // the name of field in db
    'datas' => [
		'model' => 'App\Models\Post',
		'fk_model' => 'Media',
		'attribute' => 'fk_post_id', // foreign key attribute in foreign model
		'entity' => 'gallery', // the method that defines the relationship in your foreign Model
        'id' => $this->request->post,
    ]
]
```
Possible que la table "medias" dans l'objet n'ai pas de "s" a la fin.

## Authors
* **Kryzalid** - [Kryzalid.net](https://www,kryzalid.net)
